package master.projet_note;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

import static org.junit.Assert.*;

import org.junit.Test;

import org.junit.Test;

import junit.framework.TestCase;


public class NoteTest 

//extends TestCase
{
	
	
	String chemin= new File("").getAbsolutePath()+"/src/main/java/ressources/";
	Note note;
	String nom;
	
	@Test
	public void Testcreer() {
		
		String nom1="testCreer.adoc";
		
		note = new Note();
		Creer creer1 = new Creer(note,"testCreer.adoc");
		Invocateur invocateur = new Invocateur(note);
		invocateur.exec(creer1);
		File f = new File(chemin+nom1);
		
		assertTrue(f.exists());
    }

	
	
	@Test
	public void Testsupprimer() {
		
		String nom1 = "testSupprimer.adoc";
		
		note = new Note();
		Creer creer1 = new Creer(note,nom1);
		File f = new File(chemin+nom1);
		Supprimer sup=new Supprimer(nom1);
	    assertFalse(f.exists());
		
	}	

	@Test
	public void TestEditer() throws FileNotFoundException {
		
		String nom1 = "testEditer.adoc";
		String chemin1 = new File("").getAbsolutePath()+"/src/main/java/ressources/"+nom1;

		note = new Note();
		Creer creer1 = new Creer(note,"testEditer.adoc");
		Invocateur invocateur = new Invocateur(note);
		invocateur.exec(creer1);
		
		File f1 = new File(chemin1);
		int a = f1.hashCode();
		

		PrintWriter ecrivain =  new PrintWriter(new File(chemin1));
	  ecrivain.println("bonjour, j'ai modifié le fichier courant juste pour tester la méthode hahahahahah ");
	    ecrivain.println(new Integer(36));
	    ecrivain.close();
	    
	  	 int b = f1.hashCode();
	    
	     assertEquals(a,b);
 
		
	}	
		
	

	
	}
		
		
	


