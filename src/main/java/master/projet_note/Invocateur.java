package master.projet_note;

import java.util.ArrayList;
import java.util.List;
/**
 * 
 * la classe qui invoque chaque commande pour qu elle 
 *execute un traitement specifique
 */
public class Invocateur {
	private Note note;

	Commande commande;
	//public Invocateur() {note=n;}
	public void exec(Commande commande) {
		this.commande = commande;
		commande.executer();
	}

}

