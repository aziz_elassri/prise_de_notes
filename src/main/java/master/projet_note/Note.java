
package master.projet_note;

import java.awt.Desktop;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.LineNumberReader;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import java.util.StringTokenizer;
import static org.asciidoctor.Asciidoctor.Factory;
import org.asciidoctor.SafeMode;
import org.asciidoctor.ast.DocumentHeader;
import org.asciidoctor.Asciidoctor;
/**
 * 
 * classe recepteur de l'action de chaque commande
 *
 */
public class Note {
	
	  private String nom;
	//public Date date;

	private File fichier;
	private String chemin;
	 private Date dateCreation;
	private  Map<String, String> Attributs = new HashMap<String, String>();

	/**
	 * 
	 * @param nom(titre) de la note 
	 */
	public Note(String nom) {
	// nom="";
	this.nom=nom;
	 this.fichier= new File("");
	 this.chemin = fichier.getAbsolutePath()+"/src/main/java/ressources/";
	
	}
	public Note() {
		 this.fichier= new File("");
		 this.chemin = fichier.getAbsolutePath()+"/src/main/java/ressources/";
	}
	public String getNom(){ return this.nom;}
	public void setNom(String nom) {this.nom=nom;}
	public String getChemin(){ return this.chemin;}
	public void setChemin(String ch){this.chemin=ch;}
	public File getFichier(){ return this.fichier;}
	public void setFichier(File fichier){  this.fichier=fichier;}
	
	public void creer() throws IOException{
		
		 this.fichier = new File(this.chemin+this.nom+".adoc") ; 
		
		 if(!this.fichier.exists()) {
		if(this.fichier.createNewFile()) {
			
			System.out.println("\nLa note \"" +nom+"\" est bien cree dans le chemin  "+chemin+" \n");
			PrintWriter writer;
			String dateCreation  = new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
			 this.Attributs.put(":project:"," ");
			 this.Attributs.put(":context:", " ");
			 this.Attributs.put("DateCreation", dateCreation);
				writer = new PrintWriter(fichier);
				 
		            	writer.println("= "+nom+"\n"+"nom prenom <tonMail@exemple.com>\n"
		            			+dateCreation+"\n"+":author: \n"+":email: \n"+":context: "+
		            			"\n"+":project: \n");
		            
		         writer.close();
		         
		            Asciidoctor asciidoctor = Asciidoctor.Factory.create();
		    		Map<String, Object> attributes = new HashMap<String, Object>();
		    		attributes.put("backend", "html"); 
		    		attributes.put("project", ""); 
		    		attributes.put("context", ""); 
		    		Map<String, Object> options = new HashMap<String, Object>();
		    		options.put("attributes", attributes); 
		    		options.put("in_place", false); 
		    		String outfile = asciidoctor.convertFile(this.getFichier(), options);
		
		}}
		else {
			System.out.println("\nla note \""+nom+"\" existe deja");
		}
		}
		/*try {
			PrintWriter out = new PrintWriter(new FileWriter(fichier)) ;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("Ce fichier exiqte deja");

		}*/
	/**
	 * methode pour modifier le contenu d'une note s'il exite, sinon elle va la creer
	 */
	public void editer() {
		Converter conv=new Converter();
		this.fichier  = new File(chemin+nom+".adoc");
		if(this.getFichier().exists() && !this.fichier.isDirectory())
		{
			System.out.println("\nOn va editer la note \""+nom+"\" ");
			boolean isOpen=true;

		if(Desktop.getDesktop().isSupported(java.awt.Desktop.Action.OPEN)){
		    try { java.awt.Desktop.getDesktop().open(fichier);
		 
		    
		   // File f = new File(fichier.getAbsolutePath()+"/src/main/java/ressources/"+liste[i]);
		    if(fichier.canWrite()) {}else {System.out.println("Le ");
		    isOpen=false;}
			BufferedReader reader = new BufferedReader(new FileReader(fichier)); 
			String line = null; 
			boolean isFree=false;
		
			/*while ((line = reader.readLine()) != null) { 
			if (line.contains(":project:")) { 
				this.Attributs.put(":project:", line.toString().substring(10));
			System.out.println(); 
			//System.out.println("Le fichier "+fichier.getName()+" contient le projet "+this.Attributs.get(":project:"));  
			conv.convert(nom,nom);
			 reader.close();*/

		  
		    } catch (IOException ex) {ex.printStackTrace();} 
		    

		}
		else {
		System.out.println("Création et ouverture de la note : "+nom+" ");
      //  Note note1 = new Note();
        Creer creer = new Creer(new Note(nom));
        Invocateur invocateur1 = new Invocateur();
        
        invocateur1.exec(creer);
        //indexer();
        if(Desktop.getDesktop().isSupported(java.awt.Desktop.Action.OPEN)){
		    try { java.awt.Desktop.getDesktop().open(fichier); }
		    catch (IOException ex) {ex.printStackTrace();}
		    }
        
		}
	}
		//Desktop desk=Desktop.getDesktop();
	//	try {
		//	desk.edit(new File("Sirage.adoc"));}
		
		//catch (Exception e) {System.out.println(e);}
	//}
	}
	/**
	 * va afficher la note sous format html dans le navigateur
	 * @param nom
	 */
	public void afficher(String nom) {
		nom=this.chemin+this.nom;

		try {
			java.awt.Desktop.getDesktop().open(new File(nom.substring(0, nom.lastIndexOf('.'))+".html"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
/**
 * methode qui va lister toute les notes existants dans le repertoire des ressources
 */
	public void lister() {

		File repertoire = new File(fichier.getAbsolutePath()+"/src/main/java/ressources/");
        String liste[] = repertoire.list();      
 
        if (liste != null ) {  
        	int cmp = 0;
			for (int i = 0; i < liste.length ; i++) {
				if(liste[i].equals("index.adoc") || liste[i].equals("Html") || liste[i].substring(liste[i].length()-5,liste[i].length()).equals(".html")) continue;
				if(cmp %7==0)System.out.println("\n");
				System.out.print(liste[i].substring(0, liste[i].length()-5)+"     ");
			    cmp++;
			    }
          
        } else {
            System.err.println("\nY a aucune note !");
        }

	}
	/**
	 * 
	 * @param s : le mot que l'utilisateur veut chercher, ca peut etre titre ou mot cle 
	 * @throws IOException
	 */
	public void chercher(String s) throws IOException {
	File repertoire = new File(fichier.getAbsolutePath()+"/src/main/java/ressources/");
    String liste[] = repertoire.list(); 
    int compteur=0;
		
    for (int i = 0; i < liste.length; i++) {
    	if(liste[i].equals("index.adoc") || liste[i].equals("Html") || liste[i].substring(liste[i].length()-5,liste[i].length()).equals(".html")) continue;
		  if(liste[i].subSequence(0, liste[i].lastIndexOf('.')).equals(s)) {
			 System.out.println("\n"+liste[i].substring(0, liste[i].lastIndexOf('.')));
		  		compteur++;
		  		}
      }
   if (compteur> 0) {
		  		System.out.println("\n------Nous avons trouve "+compteur+" note(s) ayant comme titre  "+s+"\n");
	
   }
	
		File file = new File(""); 
	int nbrFichiers=0;
	String noteTrouvee = null;
	 Hashtable table = new Hashtable();
	
	 String mot;
	// for() {
	for (int i = 0; i < liste.length; i++) {
		compteur=0;
		if(liste[i].equals("index.adoc") || liste[i].equals("Html")) continue;
	
		File f = new File(file.getAbsolutePath()+"/src/main/java/ressources/"+liste[i]);
	BufferedReader reader = new BufferedReader(new FileReader(f)); 
	String line = null; 
	while ((line = reader.readLine()) != null) {
		;
		 StringTokenizer st=new StringTokenizer(line);
		   while(st.hasMoreTokens()){
		      if(st.nextToken().equals(s)&&!line.contains("== ")){
		    	  noteTrouvee=liste[i];
		       compteur++;
		    }
	}
	}
	reader.close();
	if(compteur!=0) {
		nbrFichiers++;
		System.out.println(" Le fichier "+noteTrouvee+" contient le mot "+s+" "+compteur+" fois \n");
	}
	
	
	}
	System.out.println("------Nous avons trouve "+nbrFichiers+" notes qui contient le mot  "+s);

}
	/**
	 * va supprimer la note avec le fichier html correspondant
	 */
	public void supprimer() {

		//chemin = fichier.getAbsolutePath()+"/src/main/java/ressources/";
		 fichier = new File(chemin+nom+".adoc");
			File fichierHtml = new File(chemin+nom+".html");
		if(fichier.delete()&&fichierHtml.delete()) {
			System.out.println("\nLa note\""+this.getNom()+"\" est bien supprimee !");
			//indexer();
		}else {
			System.out.println("\nLa note \""+this.getNom()+"\"n'exite pas !");
		}
	}
	/**
	 * va mettre les titres de toutes les notes dans un fichier adoc selon un ordre alphabetique
	 **/

	public  void indexer() {

		File f2=new File("");
		File repertoire = new File(chemin);
	
        String liste[] = repertoire.list();   
        Arrays.sort(liste);
          
       
        ArrayList<String> ListeProject = new ArrayList<String>();
        if (liste != null) {  
        	File f = new File(chemin+"index.adoc");
        	 PrintWriter writer;
			try {
				writer = new PrintWriter(f);
				writer.println("== Index \n" + 
	        			":author: Admin\n" + 
	        			":email: admin@gemail.com\n=== Toutes les notes :");

				for (int i = 0; i < liste.length; i++) {
					if ( liste[i].equals("index.adoc"))continue;
					
					if(!(liste[i].endsWith(".html"))){					
				    	writer.println("- "+liste[i].substring(0,liste[i].lastIndexOf('.')));
				    	//Asciidoctor asciidoctor = create();
					    //    DocumentHeader header = asciidoctor.readDocumentHeader(new File(f2.getAbsolutePath()+"/src/main/java/ressources/"+liste[i]));
						//	RevisionInfo revisionInfo;
							//revisionInfo = header.getRevisionInfo();
					      //  if(!(header.getAttributes().get(":project:")==null)&&!(header.getAttributes().get("project").equals(""))) {
						//	ListeProject.add(header.getAttributes().get("project").toString());
							
					        }
					        //System.out.println(ListeProject.get(0));
				    	//File f2 = new File(fichier.getAbsolutePath()+"/src/main/java/ressources/"+liste[i]);
				
					
						}
				for(String element : ListeProject)
					System.out.println(element);/*
				/*writer.println("=== Selon l'attribut projet :");
				for(String element : ListeProject ) {
					
					writer.println(". "+element);
							for (int i = 0; i < liste.length; i++) {
								if (!liste[i].endsWith(".html") || liste[i].equals("index.adoc"))continue;
								 Asciidoctor asciidoctor = create();
							        DocumentHeader header = asciidoctor.readDocumentHeader(new File(f.getAbsolutePath()+"/src/main/java/ressources/"+liste[i]));
									if(header.getAttributes().get("project").equals(element)) {
										writer.println(".. "+liste[i]);	
									}
								
								
									
									File f3 = new File(f.getAbsolutePath()+"/src/main/java/ressources/"+liste[i]);
									BufferedReader reader2 = new BufferedReader(new FileReader(f3)); 
									String line2 = null; 
									writer.println("--------------------------------");
									while ((line2 = reader2.readLine()) != null) { 
									if (line2.contains(":project:") && line2.contains(element)) {
										writer.println(liste[i].substring(0,liste[i].lastIndexOf('.')));
										break;
									}
						}
											reader2.close();
				    	
						}
				    }
						//System.out.println("Le fichier "+liste[i].toString()+" contient le projet "+ListeProjets.get(0));  
				}

				
				    writer.close();	*/
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

        
	}
}


}