package master.projet_note;
/**
 * 
 * la classe qui represente une commande concrete par rapport au design pattern Command
 *
 */
public class Supprimer extends Commande {
private String nomFichier;
	
	
	public Supprimer(Note n) {
	//	maNote=n;
		maNote.setNom(n.getNom());
		maNote.setChemin(n.getChemin());
		maNote.setFichier(n.getFichier());
		//this.nomFichier=nomFichier;
	}
	
	public void executer() {
		
		maNote.supprimer();
		}

}
