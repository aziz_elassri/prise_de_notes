package master.projet_note;

import java.awt.GraphicsEnvironment;
import java.io.*;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import javax.swing.JOptionPane;

import org.asciidoctor.ast.Document;
import org.asciidoctor.Asciidoctor;
import org.asciidoctor.Asciidoctor.Factory;
import org.asciidoctor.Attributes;
import org.asciidoctor.Options;


/**
 * Hello world!
 *
 */
public class App 
{	
	private static Scanner sc;
	
    public static void main(String[] args) throws IOException  
    { 	
    	 Invocateur invocateur = new Invocateur();

    	 if(args.length == 0){

    		 sc = new Scanner(System.in);
				String entree;
				while (true) {
					 System.out.println("\n**************************************************************");
					 System.out.println("*                                                            *");
			    	 System.out.println("* Vueillez choisir entre les commandes ci dessous :          *");
			    	 System.out.println("*                                                            *");
			    	 System.out.println("*   -A==> Creer                                              *\n"
			    	 		          + "*   -B==> Afficher                                           *\n"
			    	 		          + "*   -C==> Editer                                             *\n"
			    	 		          + "*   -D==> Supprimer                                          *\n"
			    	 		          + "*   -E==> Lister                                             *\n"
			    	 		          + "*   -F==> Chercher                                           *\n"
			    	 		          + "*   -Q==> Exit                                               *");
			    	 System.out.println("*                                                            *");
			    	 System.out.println("**************************************************************\n>>>>");
					entree = sc.nextLine();
					
					switch (entree.toLowerCase()) {
					case "a":
					System.out.print("veuillez saisir le nom de la note (il sera son titre !) :\n\n>>>>");
					String nomNote = sc.nextLine();
					Note n=new Note(nomNote);
					Creer creer = new Creer(n);
		    		invocateur.exec(creer);
		    	
		    		/*Asciidoctor asciidoctor = Asciidoctor.Factory.create();
		    		Map<String, Object> attributes = new HashMap<String, Object>();
		    		attributes.put("backend", "html"); 
		    		attributes.put("project", ""); 
		    		attributes.put("context", ""); 
		    		Map<String, Object> options = new HashMap<String, Object>();
		    		options.put("attributes", attributes); 
		    		options.put("in_place", true); 
		    		String outfile = asciidoctor.convertFile(new File(n.getChemin()+n.getNom()), options);*/
		    		
					break;
					case "b":
						System.out.print("veuillez saisir le nom de la note a afficher :\n\n>>>>");
						String nomNoteAff = sc.nextLine();
						Afficher aff = new Afficher(nomNoteAff+".adoc");
			    		invocateur.exec(aff);
						break;
					case "c":
						System.out.print("veuillez saisir le nom de la note a modifier :\n\n>>>>");
						String nomNoteModif = sc.nextLine();
						
						Editer editer = new Editer(new Note(nomNoteModif));
			    		invocateur.exec(editer);
							
						break;
					case "d":
					System.out.print("Quelle note voulez-vous supprimer ? \n\n>>>>");
					String nomNoteSup = sc.nextLine();
					if(nomNoteSup==""||nomNoteSup==null) {
		     			JOptionPane.showMessageDialog(null, "Faut saisir le nom de la note à supprimer !", "error", JOptionPane.ERROR_MESSAGE);
		     			 System.exit(0); 
		     		}
					Supprimer supprimer = new Supprimer(new Note(nomNoteSup));
		    		invocateur.exec(supprimer);
		    		break;
					case "e":
						Lister lister=new Lister();
			    		invocateur.exec(lister);	
						break;
					
					case "f":
						System.out.print("Voulez-vous chercher selon quel critere ? \n\n>>>>"+
					"t ==> Titre\nd ==> Date\nm ==> Mots cle\n");
						Scanner sc = new Scanner(System.in);
						String choix = sc.nextLine();
						switch(choix.toLowerCase()) {
						case "t":
							if(choix==""||choix==null) {
				     			JOptionPane.showMessageDialog(null, "Faut saisir le titre de la note que vous voulez cherchez !", "error", JOptionPane.ERROR_MESSAGE);
				     			 System.exit(0); 
				     		}
				    		else if(args.length==2) {
				    			Chercher chercher=new Chercher(args[1]+" "+args[2]);
				    			invocateur.exec(chercher);
				    		}
							break;
						case "d":
							if(choix==""||choix==null) {
				     			JOptionPane.showMessageDialog(null, "Faut saisir la date creation de la note que vous voulez cherchez !", "error", JOptionPane.ERROR_MESSAGE);
				     			 System.exit(0); 
				     		}
							
							break;
						case "m":
							System.out.print("Veuillez saisir les mots cles que vous voulez cherchez dans vos notes !\n\n>>>>");
							if(choix==""||choix==null) {
				     			JOptionPane.showMessageDialog(null, "Faut saisir les mots cles que vous voulez cherchez dans vos notes !", "error", JOptionPane.ERROR_MESSAGE);
				     			 System.exit(0); 
				     		}
							else {
								String motsCherches = sc.nextLine();
								Chercher chercher=new Chercher(motsCherches);
			    			invocateur.exec(chercher);
			    			}
							break;
						default:
		JOptionPane.showMessageDialog(null, "Faut choisir entre t, d et m !", "error", JOptionPane.ERROR_MESSAGE);
		
							
						}
						break;
						default:
							JOptionPane.showMessageDialog(null, "Saisi non valide !", "error", JOptionPane.ERROR_MESSAGE);
					}
					if (entree.equals("q") || entree.equals("quite")) {
						System.exit(0);
					}
					
				
				
				}
    	 }
    	//Note note = new Note();
       
      
       
       // Editer edit = new Editer("haha30.adoc");
       // Afficher afficher1=new Afficher("hello.adoc");
		//invocateur.exec(edit);
		//invocateur.exec(afficher1);
		// InputStream stream = App.class.getResourceAsStream("/hello.adoc");
	        //System.out.println(stream != null);
		
		/*Asciidoctor asciidoctor = Asciidoctor.Factory.create();
		 Document document = asciidoctor.loadFile(new File("src/main/java/ressources/haha30.adoc"), new HashMap<String, Object>());

	        // Initialize attributes with header content
	        Map<String, Object> attributes2 = new HashMap<>(document.getAttributes());
	        System.out.println(document.getAttributes());
	        System.out.println(document.getOptions());
	        System.out.println(document.getSubstitutions());*/
	      
        /*  Lister lister1=new Lister();
	invocateur.exec(lister1);
	Supprimer sup=new Supprimer(new Note("dddd") );
	invocateur.exec(sup);
	System.out.println("---------------------------");
	invocateur.exec(lister1);
       Note.indexer();
        Creer crtest1=new Creer(new Note("ooopp 542"));
        invocateur.exec(crtest1);
        Editer editTest = new Editer(new Note("ooopp"));
        invocateur.exec(editTest);*/
    	 
        

    	//if(args.length>2) System.err.println("Trop d'arguments !");
    	 else if(args[0].equalsIgnoreCase("create") || args[0].equalsIgnoreCase("cr")) {
    	 Creer creer;
 		String chaine="";
 		if(args.length<2) {
 			JOptionPane.showMessageDialog(null, "Faut entrer le nom de la note à creer !", "error", JOptionPane.ERROR_MESSAGE);
 			 System.exit(0); 
 		}
 		if(args.length>2) {
 			String separateur = " "; 
 			StringBuilder tampon = new StringBuilder(); 
 			for (int index = 1 ; index < args.length ; index++) { 
 			    tampon.append(args[index]); 
 			    if (index != args.length - 1) { 
 			        tampon.append(separateur); // Insertion du séparateur. 
 			    } 
 			} 
 			 chaine = tampon.toString();
 			 creer=new Creer(new Note(chaine));
 			invocateur.exec(creer);
 		}   		
    		else	
    			{		
    		creer = new Creer(new Note(args[1]));
    		invocateur.exec(creer);
    			}
        }
    	else if(args[0].equalsIgnoreCase("search") || args[0].equalsIgnoreCase("s")) {
    		if(args.length<2) {
     			JOptionPane.showMessageDialog(null, "Faut entrer ce que vous voulez chercher (titre ou mots cle) !", "error", JOptionPane.ERROR_MESSAGE);
     			 System.exit(0); 
     		}
    		if(args.length<2) {
     			JOptionPane.showMessageDialog(null, "Faut saisir les mots clés que vous voulez cherchez dans vos notes !", "error", JOptionPane.ERROR_MESSAGE);
     			 System.exit(0); 
     		}else if(args.length==2) {
    			Chercher chercher=new Chercher(args[1]);
    			invocateur.exec(chercher);
    		}
     		else {		
     			String chaine="";
    			String separateur = " "; 
    			StringBuilder tampon = new StringBuilder(); 
    			for (int index = 1 ; index < args.length ; index++) { 
    			    tampon.append(args[index]); 
    			    if (index != args.length - 1) { 
    			        tampon.append(separateur); // Insertion du séparateur. 
    			    } 
    			} 
    			 chaine = tampon.toString();
    			 Chercher chercher=new Chercher(args[1]);
     			invocateur.exec(chercher);
    	
    	}
    		
    		}
         
    	
    	else if(args[0].equalsIgnoreCase("view") || args[0].equalsIgnoreCase("v")) {
    		if(args.length<2) {
     			JOptionPane.showMessageDialog(null, "Faut entrer le nom de la note à afficher !", "error", JOptionPane.ERROR_MESSAGE);
     			 System.exit(0); 
     		}
    		Afficher afficher;
    		String chaine="";
    		if(args.length>2) {		
    			String separateur = " "; 
    			StringBuilder tampon = new StringBuilder(); 
    			for (int index = 1 ; index < args.length ; index++) { 
    			    tampon.append(args[index]); 
    			    if (index != args.length - 1) { 
    			        tampon.append(separateur); // Insertion du séparateur. 
    			    } 
    			} 
    			 chaine = tampon.toString();
    			 afficher=new Afficher(chaine+".adoc");
    		}else
    		 afficher=new Afficher(args[1]+".adoc");
    		 invocateur.exec(afficher);
    	}
    	else if(args[0].equalsIgnoreCase("list") || args[0].equalsIgnoreCase("ls")) {
    		Lister lister=new Lister();
    		invocateur.exec(lister);	
    	} 
    	else if(args[0].equalsIgnoreCase("edit") || args[0].equalsIgnoreCase("e")) {
    		if(args.length<2) {
     			JOptionPane.showMessageDialog(null, "Faut entrer le nom de la note à modifier !", "error", JOptionPane.ERROR_MESSAGE);
     			 System.exit(0); 
     		}
    		Editer editer;
    		String chaine="";
    		if(args.length>2) {		
    			String separateur = " "; 
    			StringBuilder tampon = new StringBuilder(); 
    			for (int index = 1 ; index < args.length ; index++) { 
    			    tampon.append(args[index]); 
    			    if (index != args.length - 1) { 
    			        tampon.append(separateur); // Insertion du séparateur. 
    			    } 
    			} 
    			 chaine = tampon.toString();
    			 editer=new Editer(new Note(chaine));
    		}else
    		 editer=new Editer(new Note(args[1]));
    		 invocateur.exec(editer);
    	}else if(args[0].equalsIgnoreCase("delete") || args[0].equalsIgnoreCase("d")) {
    		if(args.length<2) {
     			JOptionPane.showMessageDialog(null, "Faut entrer le nom de la note à supprimer !", "error", JOptionPane.ERROR_MESSAGE);
     			 System.exit(0); 
     		}
    		Supprimer sup;
    		String chaine="";
    		if(args.length>2) {		
    			String separateur = " "; 
    			StringBuilder tampon = new StringBuilder(); 
    			for (int index = 1 ; index < args.length ; index++) { 
    			    tampon.append(args[index]); 
    			    if (index != args.length - 1) { 
    			        tampon.append(separateur); // Insertion du séparateur. 
    			    } 
    			} 
    			 chaine = tampon.toString();
    			 sup=new Supprimer(new Note(chaine));
    		}else
    		 sup=new Supprimer(new Note(args[1]));
   		 invocateur.exec(sup);
    	}else {
    		//System.err.println("Saisi non valide !");
    		JOptionPane.showMessageDialog(null, "Saisi non valide !", "error", JOptionPane.ERROR_MESSAGE);
    	}
  
      //  invocateur.exec(lister1);
    
     
  //   invocateur.exec(afficher2);
      //  note.indexer();
      //  invocateur.exec(lister1);
       
      
        /*invocateur.exec(sup);
        invocateur.exec(af1);
       
      
       invocateur.exec(lister);
       invocateur.exec(af1);
       invocateur.exec(editer);
      

        invocateur.exec(af1);
        invocateur.exec(sasa);
        invocateur.exec(lister);
        invocateur.exec(af1);
        /* System.out.println();
       invocateur.exec(lister);
       invocateur.exec(af1);

        invocateur.exec(creer2);

       invocateur.exec(creer);
       System.out.println();
       invocateur.exec(lister);
       invocateur.exec(af1);*/    
    }
    }


