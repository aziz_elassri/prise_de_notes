# Prise_de_notes


Notre projet s'inscrit dans l'UE Programmation, Génie Logiciel. 

En effet, le but du sujet est de réaliser une application ligne de commande en Java pour la prise de notes permettant d'éditer et d'organiser des notes. Ces dernières sont rédigées via le langage de balisage AsciiDoctor. 

Notre application sera manipulée via une interface en ligne de commande. 

---
s
### Manuel tilisateur 

Description de l'utilisation: 


D'abord, pour assurer la bonne exécution de notre application, veuillez vérifier que Java et Maven sont bien installés sur votre machine. 

Au lancement de l'application on se trouve avec deux modes d'interaction.  

- Le premier mode c'est lancer le programme avec des arguments. 

- Le second mode n'a pas d'argumentation et va ouvrir un interpréteur permettant d'interagir avec l'application . 

 ##### Avec des arguments

L'application offre les fonctionnalités suivantes: 

 

-Nous commençons  par la fonctionnalité "create" ou "cr" qui permet de creer des notes adoc dans le répertoire ressource. 


-Pour la deuxième fonctionnalité "edit" ou "e" qui permet de faire les modifications sur le fichier.adoc que l'on déjà dans le répertoire ressource. Dans le cas où le fichier est introuvable, il créera un nouveau fichier adoc 
 

-Pour la troisième commande, nous avons la fonctionnalité "lister"  ou "ls", nous permettant  de lister toutes les notes du repertoire ressources.
 
-La quatrième fonctionnalité c'est "delete" ou "d", qui permet de supprimer un fichier .adoc existant et aussi le fichier .html associé à .adoc. 

-Pour la cinquième fonctionnalité, il s'agit de "search" ou "s" qui permet de chercher dans une note en utilisant des mots clés dans l’ensemble de la note ou dans un élément particulier comme le titre et la date.<br/> 

-La sixième fonctionnalité c'est "Afficher" ou "a" permettant d'afficher la note en format html dans un navigateur et non pas en format .adoc<br/>

  
Nous finirons par la fonctionnalité "indexe" ou "I" permet de créer un fichier index.adoc généré qui contient des listes de notes classées par ordre alphabétique de titre. 
 
Veuillez trouver tous les fichiers créés bien enregistrés  dans le dossier ressources. 


### Manuel Technique 

Pour generer les fichier jar faut utiliser la commande suivante :

-maven install

Interface en ligne de commande 

-jar target/notes.jar: Cette instruction lance une invite de commande et attend les commandes décrites dans le manuel utilisateur.<br/>

Le diagramme ci-dessous décrit l'implementation de l'application: <br/>
(image/diagramme.png)


 


Groupe 

⋅⋅* ELAASRI Aziz 

⋅⋅* Zouame tirage Eddie 

⋅⋅* JABAR Yosra 

 

 

 
